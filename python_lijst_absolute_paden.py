import os 
def get_files_in_directory(path):
    file_path = []
    for file in os.listdir(path):
        file_path.append(os.path.join(path, file))
    return file_path
