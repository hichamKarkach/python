def bestaat_uit_bytes(ipadres):
    waarden = ipadres.split(".")
    result = True
    for elem in waarden:
        num = int(elem)
        if not (num >= 0 and num <= 255):
            result = False
    return result

