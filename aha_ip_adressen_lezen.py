def read_ips(line):
    return [int(element) for element in line.split(".")]

def read_ips():
    lijst = []
    with open("ips.txt") as fh:
        for line in fh.readlines():
            lijst.append(read_ip(line))
        return lijst
