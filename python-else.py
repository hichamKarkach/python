byte1 = 10
byte2 = 50
byte3 = 145
byte4 = 241

if byte1 < 0 or byte1 > 255 or byte2 < 0 or byte2 > 255 or byte3 < 0 or byte3 > 255 or byte4 < 0 or byte4 > 255:
    print("De vier bytes vormen geen geldige IPV4-adres")
else:
    if byte1 == 10:
        print("De vier bytes vormen een IPv4 adres in het bereik 10.0.0.0/8")
    else:
            print("De vier bytes vormen een IPv4-adrs buiten het bereik 10.0.0.0/8")
