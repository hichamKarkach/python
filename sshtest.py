import subprocess
with open("hosts_and_ports.txt") as hp_fh:
    hp_contents = hp_fh.readlines()
    for hp_pair in hp_contents:
        with open("commands.txt") as fh:
            poort = hp_pair
            host = poort.split(":", 1)[1]
            print(host)
            completed = subprocess.run("ssh ubuntussh@127.0.0.1 -p " + host, capture_output=True, shell=True, text=True, stdin=fh)
