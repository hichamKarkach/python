import python_spam_file_functie
import os

def spam_filter():
    path = input("Welke directory wil je controleren?")
    for files in os.listdir(path):
        absolutepath = os.path.join(path, files)
        if python_spam_file_functie.file_is_spam(path):
            print(f"{absolutepath}: spam")
        else:
            print(f"{absolutepath}: geen spam")
